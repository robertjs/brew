import axios from 'axios'

const punkBeer = axios.create({
  baseURL: 'https://api.punkapi.com/v2/',
  timeout: 1000,
  headers: { 'X-Custom-Header': 'foobar' }
})

export default punkBeer
