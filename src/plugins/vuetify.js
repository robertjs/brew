import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    primary: '#0A122A',
    secondary: '#804E49',
    cardbg: '#F6FDFC',
    error: '#ED6A5A',
    darktext: '#be9b7b',
    accent: '#F8F6F1'
  }

})
